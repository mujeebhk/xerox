# Android-MAS-AdvancedAuth
Advanced Auth Android library 

## Overview

To secure online transactions from Man-in-the-Middle (MITM) and other related attacks,
CA Strong Authentication provides client applications that are based on CA Mobile PKI
,CA Mobile OTP credentials and CA Mobile API Gateway.
These software credentials provide two-factor authentication and are based on the
patented Cryptographic Camouflage technique for securely storing keys.
You can develop your own client application by using this Software Development Kit (SDK).

## Project Dependency
1. Mobile API Gateway [MAG][mag] version 1.4
2. CA Auth ID Android SDK version 2.2.1
3. CA Device DNA Android SDK version 8.1.0
4. CA Mobile OTP Android SDK version 2.2.3

## Build the SDK

Compile and generate the aar file.

Go to project directory
```script
./gradlew build
```

The aar file is generated under
```
${project}/advanceAuth/build/outputs/aar
```


## How to install the SDK to Application
### Step 1: Review Prerequisites and Support

1. Download the SDK
2. Check supported versions
   - **Platform**: Android
   - **OS**: 4.4.2 to 7.0.0
   - **IDE**: Android Studio and build tools that support 4.4.2+

3. Your Admin must:
   - Install and configure: CA Mobile API Gateway (MAG), CA API Management OAuth Toolkit, and optionally CA Mobile App Services (MAS).
   - Give you a client app file with MAS values (msso_config.json).


### Step 2: Add the msso_config.json file to your project

Get the msso_config.json file(s) from your MAG Administrator, and add it to your Android app 'src/main/assets' folder. If you are using multiple MAGs, you may have more than one msso_config.json file.

**Sample msso_config.json file**

The following is an example of a valid msso_config.json file.

```json
{
  "server": {
    "hostname": "gatewayhostname.example.com",
    "port": 8443,
    "prefix": "",
    "server_certs": [
      [
        "-----BEGIN CERTIFICATE-----",
        "MIIDADCCAeigAwIBAgIIW3j/9QFwgk8wDQYJKoZIhvcNAQEMBQAwHjEcMBoGA1UEAxMTbWF0LWRl",
        "bW8ubDd0ZWNoLmNvbTAeFw0xNTA2MDkyMTQ1MzRaFw0yNTA2MDYyMTQ1MzRaMB4xHDAaBgNVBAMT",
        "E21hdC1kZW1vLmw3dGVjaC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCNkI+a",
        "7IUO1UOUCQiktVenQ8L5xHWwNIntEFBEGiMNcebjNiOr0DvOuNRO3YyIOST2zzYBFvpIYBGyfphy",
        "pIAxIXSz9spRIVpnZtQT877vW/SkAvAs66p191Pwk/FdjjKRZrcpSTMKBvn4hrPjcAkC2WqhFCWY",
        "ZsXPUwD9Ys9KcW8/8yGItzgc7xenqDHQhabzdSxhZPPC2KT03sCpV88uza3GxOob9ytuIK3YXybd",
        "BWbnm6mR266iAGsf/aW9VNqY4IuQZcTTKT0D6x2bkIbApBYfhZzJBYtT1+M9GcEgSfNC8VJDEuXg",
        "lu3cF5gUzPPPPP0/sdspJRhI93Q0k66zAgMBAAGjQjBAMB0GA1UdDgQWBBTKmoAs2euFYC4us9nv",
        "AK9o+PB//TAfBgNVHSMEGDAWgBTKmoAs2euFYC4us9nvAK9o+PB//TANBgkqhkiG9w0BAQwFAAOC",
        "AQEANXWKseYUSDEGGMoGSu9j79kc5BRiNbNBYYuccmVm5w5/I29EXFF7mN3YMcAj8YMQYi2IDKuU",
        "3hcMYLkqSbNVZWnJJrw7nzxSwSG/eC5jRupwJKSUNz3X+skUVcM7Dyd/kGzH/YLYj/n8y6z5UoBU",
        "48L/6XfQd1GcWHvsQi6SVT/FCN10DbVKZY96re4V0acKtQmRxd56MqnK1CkDaS59/k/G5XUAYAGW",
        "w2PevHDo7Wu+C+7fMclZfxTyLozHdl15kT2kMYfEUnYndot8ir7mNrkRDSbs8A4yYK6XlXT3C6sR",
        "fQuQQG1lDKJgMpsHMt/+M+WpF6NdYVq976Oasdfqsw==",
        "-----END CERTIFICATE-----"
      ]
    ]
  },
  "mag": {
    "system_endpoints": {
      "device_register_endpoint_path": "/connect/device/register",
      "device_remove_endpoint_path": "/connect/device/remove",
      "client_credential_init_endpoint_path": "/connect/client/initialize",
      "authenticate_otp_endpoint_path": "/auth/generateOTP"
    },
    "oauth_protected_endpoints": {
      "enterprise_browser_endpoint_path": "/connect/enterprise/browser",
      "device_list_endpoint_path": "/connect/device/list"
    },
    "mobile_sdk": {
      "sso_enabled": true,
      "location_enabled": true,
      "location_provider": "network",
      "msisdn_enabled": true,
      "enable_public_key_pinning": false,
      "trusted_public_pki": false,
      "trusted_cert_pinned_public_key_hashes": [],
      "client_cert_rsa_keybits": 1024
    },
    "ble": {
      "msso_ble_service_uuid": "980c2f36-bde3-11e4-8dfc-aa07a5b089db",
      "msso_ble_characteristic_uuid": "980c34cc-bde3-11r6-8dfc-aa07a5b093db",
      "msso_ble_rssi": -35
	}
  },
  "oauth": {
    "client": {
      "organization": "Example Organization Inc.",
      "description": "Example App",
      "client_name": "ExampleApp",
      "client_type": "confidential",
      "registered_by": "ExampleUser",
      "client_ids": [
        {
          "client_id": "819455f6-9a7e-4ee0-b159-f85b25758112",
          "client_secret": "",
          "scope": "openid msso phone profile address email msso_client_register msso_register mas_messaging mas_storage mas_identity",
          "redirect_uri": "https://ios.ssosdk.ca.com/ios",
          "environment": "iOS",
          "status": "ENABLED",
          "registered_by": "ExampleUser"
        }
      ]
    },
    "system_endpoints": {
      "authorization_endpoint_path": "/auth/oauth/v2/authorize",
      "token_endpoint_path": "/auth/oauth/v2/token",
      "token_revocation_endpoint_path": "/auth/oauth/v2/token/revoke",
      "usersession_logout_endpoint_path": "/connect/session/logout",
      "usersession_status_endpoint_path": "/connect/session/status"
    },
    "oauth_protected_endpoints": {
      "userinfo_endpoint_path": "/openid/connect/v1/userinfo"
    }
  },
  "custom": {
    "oauth_demo_protected_api_endpoint_path": "/oauth/v2/protectedapi/foo",
    "mag_demo_products_endpoint_path": "/protected/resource/products"
  }
}
```

### Step 3: Add the Mobile SDK to the project

1. Get the Android Mobile SDK from your Administrator.
2. Unzip the compressed file to a directory.
3. Copy the file, 'advanceAuth-\<version\>.aar' to the 'libs' directory of the project module.
4. Copy the file, 'ca-authid-android-sdk-2.2.1.jar' to the 'libs' directory of the project module.
4. Copy the file, 'ca-devicedna-android-sdk-8.1.0.jar' to the 'libs' directory of the project module.
4. Copy the file, 'ca-mobile-otp-android-sdk-2.2.3.jar' to the 'libs' directory of the project module.


### Step 4: Update the 'build.gradle' file

The 'build.gradle' file must be modified to include the Android Mobile SDK archive file ('.aar').

1. Navigate to the module that will contain the MAS integration.
2. Open the 'build.gradle' file.
3. Add a repositories section that contains the libs path.

```gradle
// MAS Integration begin
repositories {

    mavenCentral()
    flatDir {
        dirs 'libs'
    }
}
// MAS Integration end

```

4. In the 'repositories' section (usually at the bottom of the 'build.gradle' file), add a reference to the Mobile SDK archives, and the 2 BouncyCastle libraries. If you are using the default Login Dialog from MASUI, you will also need to include the QRCode third party library.

```gradle
dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    testCompile 'junit:junit:4.12'
    compile 'com.android.support:appcompat-v7:23.1.1'

    compile 'com.ca:mas:1.4'

}
```

5. Save the 'build.gradle' file.


### Step 5: Update the 'AndroidManifest.xml' file

You need to add permissions to your project.

1. In the app module of your project, open the file 'AndroidManifest.xml'.
2. If not already present, add the following permissions to the top of the 'AndroidManifest.xml' file. Add the permissions after the opening manifest element, and before the opening application element:

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.ca.mas"
    android:versionCode="0"
    android:versionName="0">

<uses-permission android:name="android.permission.INTERNET" />

<application android:allowBackup="true">
    <!-- rest of the application definition ... //-->
</application>

</manifest>
```
