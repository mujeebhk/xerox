package com.ca.apim.mas.authid;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ca.mas.core.context.DeviceIdentifier;
import com.ca.mas.foundation.MAS;

/**
 * Created by mujmo02 on 04/10/17.
 */

public class MASAuthIDUtil {
    private static final String PREFS_NAME = "DEVICE_ID_AUTHID";

    final static private String TAG = "MASAuthIDUtil";

    static void persistData(String key, String value) {
        key = key.toLowerCase();
        Log.d(TAG, "Persisting data  Key :" + key + "  value :" + value);


        SharedPreferences settings = MAS.getCurrentActivity().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);

        // Commit the edits!
        editor.commit();
    }

    static String getPersistedData(String key) {
        key = key.toLowerCase();
        SharedPreferences settings = MAS.getCurrentActivity().getSharedPreferences(PREFS_NAME, 0);
        Log.d(TAG, "Get Persisting data  Key :" + key + "  value :" + settings.getString(key, null));
        return settings.getString(key, null);

    }

    static void deleteAllEntriesInPersistedData() {
        SharedPreferences settings = MAS.getCurrentActivity().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();

    }

    static void deleteEntryInPersistedData(String key) {
        SharedPreferences settings = MAS.getCurrentActivity().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();

        Log.d(TAG, "deleteEntryInPersistedData  Key :" + key);

    }
}
