package com.ca.apim.mas.authid;

import android.content.Context;

import com.arcot.aid.lib.AIDException;
import com.arcot.aid.lib.Account;
import com.ca.mas.foundation.MAS;
import com.ca.mas.foundation.MASCallback;
import com.ca.mas.foundation.MASUser;

import org.json.JSONObject;

/**
 * The top level MASAdvancedAuth object represents the Mobile App Services Advanced Auth SDK in its entirety.
 * It is the front facing class where many of the configuration settings for the SDK as a whole
 * can be found and utilized.
 */


public class MASAuthID {

    private static AID_AID aid_aid;

    Context context;

    private static MASAuthID instance;

    private MASAuthID() {


    }

    /*This returns MASAdvanced instance
     */
    public static MASAuthID getInstance() {
        if (MAS.getContext() == null) {
            throw new IllegalStateException(MASAuthIDConsts.MAS_INITIALIZATION_ERROR);
        }
        if (aid_aid == null) {
            aid_aid = new AID_AID(MAS.getContext());
        }
        if (instance == null) {
            instance = new MASAuthID();
        }
        return instance;
    }

    /**
     * This Method allows the user to authenticate with the user credentials and returns the SMSESSION Token back to the application.
     *
     * @param userID   The userID of the user account.
     * @param pin      The Password of the user account.
     * @param callback The callback function to return the response message back to the application
     */
    @Deprecated
    public void loginWithAIDSMSession(final String userID, final String pin, final MASCallback<JSONObject> callback) {
/*
        if (MASUser.getCurrentUser() != null) {
            if (MASUser.getCurrentUser().isAuthenticated()) {
                MASUser.getCurrentUser().logout(new MASCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        aid_aid.loginWithAIDSMSession(userID, pin, callback);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);
                    }
                });
            }
        } else {
            aid_aid.loginWithAIDSMSession(userID, pin, callback);
        }*/
        loginWithAIDGetSMSession(userID, pin, false, callback);

    }


    /**
     * This Method allows the user to authenticate with the user credentials and returns the SMSESSION Token back to the application.
     *
     * @param userID     The userID of the user account.
     * @param pin        The Password of the user account.
     * @param doMAGLogin Setting this value as true will also do the user MAG Login
     * @param callback   The callback function to return the response message back to the application
     */
    public void loginWithAIDGetSMSession(final String userID, final String pin, final boolean doMAGLogin, final MASCallback<JSONObject> callback) {

        if (MASUser.getCurrentUser() != null) {
            if (MASUser.getCurrentUser().isAuthenticated()) {
                MASUser.getCurrentUser().logout(new MASCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        aid_aid.loginWithAIDGetSMSession(userID, pin, doMAGLogin, callback);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);
                    }
                });
            }
        } else {
            aid_aid.loginWithAIDGetSMSession(userID, pin, doMAGLogin, callback);
        }
    }

    /**
     * This Method allows the user to authenticate with the user credentials and log in the user with id_Token.
     *
     * @param userID   The userID of the user account.
     * @param pin      The Password of the user account.
     * @param callback The callback function to return the response message back to the application.
     */
    public void loginWithAID(final String userID, final String pin, final MASCallback<MASUser> callback) {

        if (MASUser.getCurrentUser() != null && MASUser.getCurrentUser().isAuthenticated()) {
            //if (MASUser.getCurrentUser().isAuthenticated()) {
            MASUser.getCurrentUser().logout(new MASCallback<Void>() {
                @Override
                public void onSuccess(Void result) {
                    aid_aid.loginWithAID(userID, pin, callback);
                }

                @Override
                public void onError(Throwable e) {
                    callback.onError(e);
                }
            });
            /*} else {
                aid_aid.loginWithAID(userID, pin, callback);
            }*/
        } else {
            aid_aid.loginWithAID(userID, pin, callback);
        }
    }

    /**
     * This method creates and saves the account based on given parameters and returns that account.
     *
     * @param b64aid    The base64 authID of the user account.
     * @param namespace The namespace of the user account.
     * @param deviceID The deviceID to be associated with this credential
     */

    public AIDAccount provisionAIDAccount(String b64aid, String namespace, String deviceID) throws Exception {
        try {
            Account account = aid_aid.provisionAccount(b64aid, namespace, deviceID);
            AIDAccount aidAccount = null;
            if (account != null) {
                aidAccount = new AIDAccount(account);
            }
            return aidAccount;
        } catch (AIDException e) {
            throw new MASAuthIDException(MASAuthIDConsts.STRONG_AUTHENTICATION_ERROR, "-", e.getMessage(), MASAuthIDConsts.REASON_CODE_0, AID_AID.mapErrorCode(e.getCode()));
        }
    }

    /**
     * This method fetches and returns the account object of the userID.
     *
     * @param userID The userID of the user account.
     */
    public AIDAccount getAIDAccount(String userID) throws MASAuthIDException {
        try {
            Account account = aid_aid.getAIDAccount(userID);
            AIDAccount aidAccount = null;
            if (account != null) {
                aidAccount = new AIDAccount(account);
            }
            return aidAccount;
        } catch (AIDException e) {
            throw new MASAuthIDException(MASAuthIDConsts.STRONG_AUTHENTICATION_ERROR, "-", e.getMessage(), MASAuthIDConsts.REASON_CODE_0, AID_AID.mapErrorCode(e.getCode()));
        }
    }

    /**
     * This method fetches and returns array of all the accounts provisioned in the application.
     */
    public AIDAccount[] getAllAIDAccounts() throws MASAuthIDException {
        try {
            Account[] accounts = aid_aid.getAllAIDAccounts();

            AIDAccount[] aidAccounts = null;
            if (accounts != null && accounts.length > 0) {
                aidAccounts = new AIDAccount[accounts.length];
                for (int i = 0; i < accounts.length; i++) {
                    aidAccounts[i] = new AIDAccount(accounts[i]);
                }
            }

            return aidAccounts;
        } catch (AIDException e) {
            throw new MASAuthIDException(MASAuthIDConsts.STRONG_AUTHENTICATION_ERROR, "-", e.getMessage(), MASAuthIDConsts.REASON_CODE_0, AID_AID.mapErrorCode(e.getCode()));
        }
    }

    /**
     * This method fetches and returns all the accounts provisioned in the application which has the same nameSpace provided.
     *
     * @param nameSpace of the user accounts.
     */
    private AIDAccount[] getAllAIDAccounts(String nameSpace) throws MASAuthIDException {
        try {
            Account[] accounts = aid_aid.getAllAIDAccounts();

            AIDAccount[] aidAccounts = null;
            if (accounts != null && accounts.length > 0) {
                aidAccounts = new AIDAccount[accounts.length];
                for (int i = 0; i < accounts.length; i++) {
                    aidAccounts[i] = new AIDAccount(accounts[i]);
                }
            }

            return aidAccounts;

        } catch (AIDException e) {
            throw new MASAuthIDException(MASAuthIDConsts.STRONG_AUTHENTICATION_ERROR, "-", e.getMessage(), MASAuthIDConsts.REASON_CODE_0, AID_AID.mapErrorCode(e.getCode()));
        }
    }

    /**
     * This method removes the account provisioned in the memory.
     *
     * @param userID The userID of the account to be removed.
     */
    public void removeAIDAccount(String userID) throws MASAuthIDException {
        try {
            aid_aid.removeAIDAccount(userID);
        } catch (AIDException e) {
            throw new MASAuthIDException(MASAuthIDConsts.STRONG_AUTHENTICATION_ERROR, "-", e.getMessage(), MASAuthIDConsts.REASON_CODE_0, AID_AID.mapErrorCode(e.getCode()));
        }
    }

    private String signWithAIDAccount(String challenge, String userID, String pin) throws MASAuthIDException {
        try {
            return aid_aid.signWithAccount(challenge, userID, pin);
        } catch (AIDException e) {
            throw new MASAuthIDException(MASAuthIDConsts.STRONG_AUTHENTICATION_ERROR, "-", e.getMessage(), MASAuthIDConsts.REASON_CODE_0, AID_AID.mapErrorCode(e.getCode()));
        }
    }

    public void close() {
        aid_aid.close();
    }
}
