package com.example.ras;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ca.apim.mas.authid.MASAuthID;
import com.ca.apim.mas.authotp.MASAOTPUtil;
import com.ca.apim.mas.authotp.MASAuthOTP;
import com.ca.mas.foundation.MAS;
import com.ca.mas.foundation.MASCallback;
import com.ca.mas.foundation.MASSessionUnlockCallback;
import com.ca.mas.foundation.MASUser;
import com.example.ras.util.AppUtil;
import com.example.ras.util.ApplicationConstants;
import com.google.common.io.LineReader;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity {



    // UI references.
    private AutoCompleteTextView authIdUsername, authOtpUsernameactv;
    private EditText authIdPwd;
    Activity context;
    TextView errMsg;
    ProgressDialog dialog;

    LinearLayout unlockSessionLL;
    Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_login);

        MAS.start(this, true);
        MAS.debug();
        context = this;

        unlockSessionLL = (LinearLayout)findViewById(R.id.loginWithFingerPrintll) ;
        unlockSessionLL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                unlockDevice();
            }
        });
        btnLogin = (Button)findViewById(R.id.btnlogin);
        TextView registerButton = (TextView) findViewById(R.id.registertv);
        registerButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO Change this
                Intent intent = new Intent(context, SelectRegistrationCredActivity.class);
                startActivity(intent);
            }
        });


        TextView contactUstv = (TextView) findViewById(R.id.contactUstv);
        contactUstv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationConstants.CONTACT_US_URL));
                startActivity(browserIntent);
            }
        });

        TextView branchestv = (TextView) findViewById(R.id.branchestv);
        branchestv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationConstants.BRANCHES_NEARBY_URL));
                startActivity(browserIntent);
            }
        });

        TextView atmtv = (TextView) findViewById(R.id.atmtv);
        atmtv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationConstants.ATM_NEARBY_URL));
                startActivity(browserIntent);
            }
        });

        if (isSessionLocked()) {
            unlockSessionLL.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.GONE);

        } else {
            unlockSessionLL.setVisibility(View.GONE);
            btnLogin.setVisibility(View.VISIBLE);
        }





        dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Please wait...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(true);

        super.onCreate(savedInstanceState);

    }






    public void onLogin(View view){
        if(!AppUtil.isNetworkAvailable(context)){
            AppUtil.showAlertDialog(context, "Please check the internet connectivity.", "Network Error", false);
            return;
        }
        fidoLogin();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event);
    }

    /**
     * Layout ID of the screen to be shown before the system authentication screen.
     *
     * @return
     */
    @LayoutRes
    protected int getLayoutId() {
        return R.layout.activity_session_unlock_login;
    }




    @Override
    protected void onResume() {
        super.onResume();
        if(MASUser.getCurrentUser() != null && MASUser.getCurrentUser().isAuthenticated()){
            startHomeActivity(true);
            return;
        }
    }




    @Override
    protected void onStop() {
        super.onStop();
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    private void fidoLogin() {
        dialog.show();
        MASUser.login(new MASCallback<MASUser>() {
            @Override
            public void onSuccess(MASUser result) {
                dialog.dismiss();
                finish();
                startHomeActivity(false);

            }
            
            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AppUtil.showAlertDialog(context,"Failed to Login","Login Error", false);
                    }
                });
            }
        });
    }


    private void startHomeActivity(boolean isResumed){
        Intent intent = new Intent(context, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if(!isResumed)
         putDataInIntentBundle(intent, "Login Successful");
        startActivity(intent);


    }





    //Session Unlocking feature
    public final static int SESSION_UNLOCK_CODE = 0x1000;

    private boolean isSessionLocked() {
        try {
            MASUser user = MASUser.getCurrentUser();
            if (user != null) {
                return user.isSessionLocked();
            }
        } catch (Exception e) {

        }
        return false;
    }

    private void unlockDevice() {
        MASUser user = MASUser.getCurrentUser();
        if (user != null) {
            user.unlockSession(getUnlockCallback());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == getFingerprintRequestCode()) {
            if (resultCode == RESULT_OK) {
                onAuthenticationSuccess();
            } else if (resultCode == RESULT_CANCELED) {
                onAuthenticationCancelled();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public MASSessionUnlockCallback<Void> getUnlockCallback() {
        return new MASSessionUnlockCallback<Void>() {
            @Override
            public void onSuccess(Void result) {
                Intent intent = new Intent(context, HomeActivity.class);
                putDataInIntentBundle(intent, "App Unlocked");
                startActivity(intent);
                //setResult(getResultCode());
                //finish();

                //fidoLogin();
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            @TargetApi(23)
            public void onUserAuthenticationRequired() {
                launchKeyguardIntent();
            }
        };
    }

    private void launchKeyguardIntent() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        Intent intent = keyguardManager.createConfirmDeviceCredentialIntent(getAuthenticationTitle(),
                getAuthenticationDescription());
        if (intent != null) {
            startActivityForResult(intent, getFingerprintRequestCode());
        }
    }

    /**
     * Value for the authentication screen title.
     *
     * @return
     */
    protected String getAuthenticationTitle() {
        return "Confirm your pattern";
    }

    /**
     * Value for the authentication screen description.
     *
     * @return
     */
    protected String getAuthenticationDescription() {
        return "Use a fingerprint or enter the current PIN";
    }

    /**
     * Called when a user successfully authenticates.
     */
    protected void onAuthenticationSuccess() {
        MASUser.getCurrentUser().unlockSession(getUnlockCallback());
    }

    /**
     * Called when a user cancels the authentication screen.
     */
    protected void onAuthenticationCancelled() {
    }

    /**
     * The delay between showing the splash image before the authentication screen appears.
     *
     * @return
     */
    protected int getAuthenticationScreenDelay() {
        return 1000;
    }

    /**
     * The ID for retrieving fingerprint results.
     *
     * @return
     */
    protected int getFingerprintRequestCode() {
        return SESSION_UNLOCK_CODE;
    }

    /**
     * The ID for startActivityForResult() results.
     *
     * @return
     */
    protected int getResultCode() {
        return SESSION_UNLOCK_CODE;
    }

}
