package com.example.ras;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Toast;

import com.arcot.aid.lib.AIDException;
import com.ca.apim.mas.authid.MASAuthID;
import com.ca.apim.mas.authid.MASAuthIDException;
import com.ca.apim.mas.authotp.MASAuthOTP;
import com.ca.apim.mas.authotp.MASAuthOTPException;
import com.ca.mas.core.error.TargetApiException;
import com.ca.mas.fido.ui.MASFidoLoginActivity;
import com.ca.mas.foundation.MASCallback;
import com.ca.mas.foundation.MASUser;
import com.example.ras.util.ApplicationConstants;

/**
 * Created by nikru01 on 11/3/2017.
 */

public class RASLoginActivity extends MASFidoLoginActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!ApplicationConstants.FIDO_LOGIN) {
            View fidoButton = findViewById(R.id.activity_mas_fido_login_button_login);
            fidoButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                        Toast toast = Toast.makeText(RASLoginActivity.this, getResources().getString(R.string.fidologindisabled), Toast.LENGTH_SHORT);
                        toast.show();
                }
            });
        }
    }

    public void logingWithAuthId(View view){
        TextInputEditText usernameEt = (TextInputEditText)findViewById( R.id.activity_mas_login_edit_text_username);
        String username = usernameEt.getText().toString();
        TextInputEditText passwordEt = (TextInputEditText)findViewById( R.id.activity_mas_login_edit_text_password);
        String password = passwordEt.getText().toString();

        MASAuthID.getInstance().loginWithAID(username+ ApplicationConstants.ORGNAME + ApplicationConstants.NAMESPACE, password, new MASCallback<MASUser>() {
            @Override
            public void onSuccess(MASUser result) {
                finish();
            }

            @Override
            public void onError(final Throwable throwable) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String msg = throwable.getMessage();
                        if (throwable.getCause() instanceof TargetApiException) {
                            msg = ((TargetApiException) throwable.getCause()).getResponse().getBody().getContent().toString();
                        } else if (throwable.getCause() instanceof MASAuthIDException) {
                            try {
                                msg = (String)((MASAuthIDException) throwable.getCause()).getResponse().get("error_details");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (throwable.getCause() instanceof AIDException) {
                            msg =  throwable.getCause().getMessage();
                        }else {
                            msg = "" + throwable.getMessage();
                        }
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

    }

    public void logingWithOTP(View view){
        TextInputEditText usernameEt = (TextInputEditText)findViewById( R.id.activity_mas_login_edit_text_username);
        String username = usernameEt.getText().toString();
        TextInputEditText passwordEt = (TextInputEditText)findViewById( R.id.activity_mas_login_edit_text_password);
        String password = passwordEt.getText().toString();

        MASAuthOTP.getInstance().loginWithAOTP(username + "::" + ApplicationConstants.ORGNAME+ "::" + ApplicationConstants.NAMESPACE,
                password, new MASCallback<MASUser>() {
            @Override
            public void onSuccess(MASUser result) {
                finish();
            }

            @Override
            public void onError(Throwable throwable) {


                String msg = throwable.getMessage();
                if (throwable.getCause() instanceof TargetApiException) {
                    msg = ((TargetApiException) throwable.getCause()).getResponse().getBody().getContent().toString();
                } else if (throwable.getCause() instanceof MASAuthOTPException) {
                    try {
                        msg = (String)((MASAuthOTPException) throwable.getCause()).getResponse().get("error_details");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    msg = "" + throwable.getMessage();
                }
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

            }
        });
    }


}
