package com.example.ras.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import com.ca.mas.core.error.TargetApiException;
import com.example.ras.LoginActivity;
import com.example.ras.R;
import com.example.ras.SelectRegistrationCredActivity;
import com.example.ras.error.ErrorObject;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mujmo02 on 8/25/2017.
 */

public class AppUtil {

    private static final String AA_ORGNAME = "AA_ORGNAME";
    private static final String AA_NAMESPACE = "AA_NAMESPACE";
    private static final String FIDO_LOGIN = "FIDO_LOGIN";
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    public static void initAppConstants (Activity activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        ApplicationConstants.ORGNAME = sharedPref.getString(AA_ORGNAME, activity.getResources().getString(R.string.aa_orgname));
        ApplicationConstants.NAMESPACE = sharedPref.getString(AA_NAMESPACE, activity.getResources().getString(R.string.aa_namespace));
        ApplicationConstants.FIDO_LOGIN = sharedPref.getBoolean(FIDO_LOGIN, false);
        ApplicationConstants.CONTACT_US_URL = activity.getResources().getString(R.string.contactus_url);
        ApplicationConstants.ATM_NEARBY_URL= activity.getResources().getString(R.string.atm_url);
        ApplicationConstants.BRANCHES_NEARBY_URL = activity.getResources().getString(R.string.branches_url);

    }

    public static void updateAppConstants (Activity activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(AA_ORGNAME, ApplicationConstants.ORGNAME);
        editor.putString(AA_NAMESPACE, ApplicationConstants.NAMESPACE);
        editor.putBoolean(FIDO_LOGIN, ApplicationConstants.FIDO_LOGIN);
        editor.commit();
    }

    public static ErrorObject getMASErrorMessage (Throwable throwable) {
        ErrorObject response = new ErrorObject();
        if (throwable.getCause() instanceof TargetApiException) {
            JSONObject errorDataJson =  (JSONObject) ((TargetApiException) (throwable.getCause())).getResponse().getBody().getContent();
            try {
                response.setErrorDetail(errorDataJson.getString("error_details"));
                response.setErrorDesc(errorDataJson.getString("error_description"));
                response.setResponseCode(errorDataJson.getString("response_code"));
                response.setReasonCode(errorDataJson.getString("reason_code"));
                response.setError(errorDataJson.getString("error"));
                return response;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            response.setErrorDetail(throwable.getMessage());
            response.setErrorDesc(throwable.getMessage());
            response.setResponseCode(null);
            response.setReasonCode(null);
            response.setError(null);
        }
        return response;
    }

    public static boolean isNetworkAvailable(Context context) {
        try {
            int[] networkTypes = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            for (int networkType : networkTypes) {
                NetworkInfo netInfo = cm.getNetworkInfo(networkType);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public static void showAlertDialog(final Context context, String msg, String title, final boolean doFinish) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                        if (context instanceof SelectRegistrationCredActivity  && doFinish) {
                            context.startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            ((Activity) context).finish();
                        }

                    }
                })
                .show();
    }

}
