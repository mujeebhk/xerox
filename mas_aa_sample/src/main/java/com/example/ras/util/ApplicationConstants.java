package com.example.ras.util;

/**
 * Created by mujmo02 on 8/25/2017.
 */

public class ApplicationConstants {

    public static String ORGNAME;

    public static String NAMESPACE;

    public static boolean FIDO_LOGIN;

    public static String USERNAME;

    public static boolean GOTO_DEVICE_HOME_SCREEN = false;

    public static boolean IS_FUND_TRANSFER = false;


    public enum REGISTERING_FOR { AUTH_ID, AUTH_OTP}
    public static REGISTERING_FOR REG_TYPE;

    public static String CONTACT_US_URL;
    public static String ATM_NEARBY_URL;
    public static String BRANCHES_NEARBY_URL;

    public static String getORGNAME() {
        return ORGNAME;
    }

    public static void setORGNAME(String ORGNAME) {
        ApplicationConstants.ORGNAME = ORGNAME;
    }

    public static String getNAMESPACE() {
        return NAMESPACE;
    }

    public static void setNAMESPACE(String NAMESPACE) {
        ApplicationConstants.NAMESPACE = NAMESPACE;
    }


}
