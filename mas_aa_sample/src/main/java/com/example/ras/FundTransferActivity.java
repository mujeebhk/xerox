package com.example.ras;

import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ca.apim.mas.riskanalysis.MASRiskAnalysis;
import com.ca.mas.core.error.TargetApiException;
import com.ca.mas.fido.MASFido;
import com.ca.mas.fido.exception.MASFidoException;
import com.ca.mas.foundation.MAS;
import com.ca.mas.foundation.MASCallback;
import com.ca.mas.foundation.MASRequest;
import com.ca.mas.foundation.MASResponse;
import com.ca.mas.foundation.MASResponseBody;
import com.ca.mas.foundation.MASUser;
import com.example.ras.util.AppUtil;
import com.example.ras.util.ApplicationConstants;
import com.google.gson.Gson;
import com.nimbusds.jose.util.Base64URL;

import org.bouncycastle.util.encoders.Base64Encoder;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import sun.misc.BASE64Encoder;

import static com.example.ras.util.ApplicationConstants.FIDO_LOGIN;

public class FundTransferActivity extends BaseActivity {

    Spinner beneficiary, payTos;
    EditText amount;
    Button transfer;
    Activity context;
    boolean stepUpDone = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_fund_transfer);
        context = this;
        beneficiary = (Spinner) findViewById(R.id.beneficiarys);
        payTos = (Spinner) findViewById(R.id.payTos);
        amount = (EditText) findViewById(R.id.amountet);
        transfer = (Button) findViewById(R.id.transferBtn);
        transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String beneficiaryStr = beneficiary.getSelectedItem().toString();
                String payTo = payTos.getSelectedItem().toString();
                String amountStr = amount.getText().toString();


                String regexStr = "^[0-9]*$";

                if (amountStr == null || "".equals(amountStr) || !amountStr.matches(regexStr)) {
                    showToastMessage("Please enter a valid transfer amount");
                    return;
                }

                if (ApplicationConstants.IS_FUND_TRANSFER && "--Choose--".equals(beneficiaryStr) ) {
                        showToastMessage("Please select a beneficiary");
                          return;
                }
                if (!ApplicationConstants.IS_FUND_TRANSFER &&"--Choose--".equals(payTo)) {
                        showToastMessage("Please select who you want to pay");
                    return;
                }

                makeAPICall(amountStr);

            }
        });
        LinearLayout beneficiarysll = (LinearLayout) findViewById(R.id.beneficiarysll);
        LinearLayout paytoll = (LinearLayout) findViewById(R.id.paytoll);
        TextView fundTransferHeadertv = (TextView) findViewById(R.id.fundTransferHeadertv);

        if (ApplicationConstants.IS_FUND_TRANSFER) {
            fundTransferHeadertv.setText("FUND TRANSFER");
            beneficiarysll.setVisibility(View.VISIBLE);
            paytoll.setVisibility(View.GONE);
        } else {
            fundTransferHeadertv.setText("PAY BILLS");
            beneficiarysll.setVisibility(View.GONE);
            paytoll.setVisibility(View.VISIBLE);
        }
        super.onCreate(savedInstanceState);

    }


    private void makeAPICall(String amountStr) {

        JSONObject json = new JSONObject();
        try {

            String userId = MASUser.getCurrentUser().getId();

            if (userId.contains(":")) {
                String[] splitUserName = userId.split(":");
                userId = splitUserName[1];
            }

            json.put("userName", userId);
            json.put("orgName", ApplicationConstants.ORGNAME);

            json.put("deviceIDValue", MASRiskAnalysis.getInstance().getDeviceId());
        } catch (JSONException e) {
            e.printStackTrace();
            showToastMessage(e.getMessage());
        }

        Base64URL jsonEncoded = Base64URL.encode(json.toString());

        final MASRequest request = new MASRequest.MASRequestBuilder(getRiskAPIUrl()).header("X-CA-RAS-RiskDetails", jsonEncoded.toString()).build();

        MAS.invoke(request, new MASCallback<MASResponse<Object>>() {
            @Override
            public void onSuccess(MASResponse<Object> result) {



                Log.d(TAG, "Success");
                Map headers = result.getHeaders();
                List cookiesList = (List)headers.get("Set-Cookie");
                String cookiesString  = (String) cookiesList.get(0);

                String cookies[] = cookiesString.split(";");
                String deviceId = "";
                for (int i=0; i<cookies.length; i++) {
                    if (cookies[i].startsWith("deviceId=")) {
                        deviceId = cookies[i].replace("deviceId=", "");
                        MASRiskAnalysis.getInstance().setDeviceId(deviceId);
                        break;
                    }
                }

                MASResponseBody<Object> body = result.getBody();
                Object content = body.getContent();
                if (body.getContent().toString().contains("INCREASEAUTH")) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                try {
                                    if (!ApplicationConstants.FIDO_LOGIN) {
                                        showToastMessage("FIDO is disabled. Unable to perform step up authentication. Transaction failed. ");
                                        return;
                                    }
                                    MASFido.loginWithFido(MASUser.getCurrentUser().getId(), new MASCallback<JSONObject>() {
                                        @Override
                                        public void onSuccess(JSONObject result) {
                                            stepUpDone = true;
                                            showSuccessMessage();
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                            showToastMessage("Biometric Authentication Error");
                                            Log.e(TAG, e.getMessage());
                                        }
                                    });

                                } catch (MASFidoException e) {
                                    showToastMessage(e.getMessage());
                                    Log.e(TAG, e.getMessage());
                                }
                            }
                        });

                } else {
                    showSuccessMessage();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "failure");

                if (e.getCause() instanceof TargetApiException) {
                    int responseCode = ((TargetApiException) e.getCause()).getResponse().getResponseCode();
                    if (403 == responseCode) {
                        showToastMessage("Transaction Denied");
                        return;
                    }
                }

                    showToastMessage(e.getMessage());

            }
        });
    }

    private URI getRiskAPIUrl() {
        try {
            return new URI("/riskDemo/resource");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    void showSuccessMessage () {
        String message;
        final String msg;
        if (ApplicationConstants.IS_FUND_TRANSFER) {
            message = "Fund Transfer Successful";
        } else {
            message = "Bill Payment Successful";
        }

        if (stepUpDone) {
            msg  = "Step Up Auth Successful And " + message;
        } else {
            msg = message;
        }
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AppUtil.showAlertDialog(context,msg, "Success", false);
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        //finish();
    }
}
