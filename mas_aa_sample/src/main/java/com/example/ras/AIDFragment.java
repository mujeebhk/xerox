
package com.example.ras;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.arcot.aid.lib.AIDException;
import com.ca.apim.mas.authid.AIDAccount;
import com.ca.apim.mas.authid.MASAuthID;
import com.ca.apim.mas.authid.MASAuthIDException;
import com.ca.mas.core.MobileSsoConfig;
import com.ca.mas.core.conf.Config;
import com.ca.mas.core.error.TargetApiException;
import com.ca.mas.foundation.MAS;
import com.ca.mas.foundation.MASAuthenticationListener;
import com.ca.mas.foundation.MASCallback;
import com.ca.mas.foundation.MASDevice;
import com.ca.mas.foundation.MASOtpAuthenticationHandler;
import com.ca.mas.foundation.MASRequest;
import com.ca.mas.foundation.MASResponse;
import com.ca.mas.foundation.MASUser;
import com.ca.mas.foundation.auth.MASAuthenticationProviders;
import com.example.ras.util.ApplicationConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Sample to show a TestCases.
 */
public class AIDFragment extends Fragment {

    MASAuthID MASAuthID = null;
    private String authToken_key = "authToken";
    private String error_key = "error_details";
    EditText tokenIDResponse = null;
    EditText protectedAPI_Response = null;
    static String USERNAME_POSTFIX;// = "cadir1ca";

    CheckBox doMAGLogincb;
    boolean doMAGLogin = true;
    //String base64aid = "";//"MIIDEgIBAAwIVVNFUjAwMDAwggL/MIIC+wIBAQwGQVJDQVJEoIHVMB0GCmCGSAGG+UYJAQEwDwQAAgEBAwEAAgFoAgIFKASBszCBsAIBADAOBgpghkgBhvlGCQIABQAEgZowgZcCgYEAs3pIpaoSwJOjcxQGXC16AzcuO34BhHaZH62j9sjeneDG2uQwFBpDww/MDGZGB0/7z8OrxecQwTKlOD1G+AkaJrc6HXtgBxCcd+lj3YQlYy3oUTuZysiWJfdfWBLSsJ4Yz5yJBO5JTh5Kkm9GDZ+cnozTWL9z8QvFEop5+lyqgQMCEQCpILqDHMvMQ9BEoa4pWsKlooIB2jCCAUOgAwIBAgIQMDafdDqCTxq5AU0Ps4ImpTANBgkqhkiG9w0BAQQFADAgMQ4wDAYDVQQLDAVBcmNvdDEOMAwGA1UECgwFQXJjb3QwHhcNMTcwNDA3MDAwMDAxWhcNMzcwNDAyMjM1OTU5WjA3MREwDwYKCZImiZPyLGQBAQwBMjEPMA0GA1UECgwGQ0FESVIxMREwDwYDVQQDDAhVU0VSMDAwMDCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAxaGub8sjonW0TZyv/dl2K0527KiP8HNM4w3cRFxCb9/HaakRniy3l9zhXcpLcQDlYMly7bLx3GjYesVq4FmNcoeQDZasSl9YzpeIv/zL7kAruBSNqF8C5+lWLjLqW+GkysI1zd+uFqgq8+ahs8Zs2ytbavhP1d3N7fD+YUjk8ssCAwEAATANBgkqhkiG9w0BAQQFAAOBgQAI16VCvI8c4RzTsm+LA2clNdDOcXkM5J0yhazQEZzGB3grONZTvCUJoXr0zXAtcephITOUHS57sTvNlegw60emJHWlmHRZ8a4UU+DOpfuSl+gmEiyIZyxdRBe36fUsOQqq3nk40+STiYt8xTsz4QbExB6Ww1NbuRxK5o+cFyYP6KM4MA0WA09yZwQGQ0FESVIxMBIWBnVzZXJpZAQIVVNFUjAwMDAwExYOYWlkYXR0cnZlcnNpb24EATAwAA==";
    String base64aid = "MIIDCwIBAAwFU0NPVFQwggL7MIIC9wIBAQwGQVJDQVJEoIHVMB0GCmCGSAGG+UYJAQEwDwQAAgEBAwEAAgFoAgIFKASBszCBsAIBADAOBgpghkgBhvlGCQIABQAEgZowgZcCgYEAnzp4hxoQWyux/nncTNeroBpdYtxTNqMjHoXRAcOJIwFTnJ64Jj7PfYy/+iNxv5YZ3+0yqS/dyhtJt5mQQv7/kAFhsvhIlzvWaVgBFmgqRyfSzkpQPBZ/CHvvlGvohsRaDb8NLp+wA06L4kA1Bbmg95Yy6FqknCdm7ZlSjwmMSj8CEQDJB2E5e5OzAyRrOi5M7M6ZooIB2TCCAUKgAwIBAgIQPVMpq8sCShat6RlJG2rsQzANBgkqhkiG9w0BAQQFADAgMQ4wDAYDVQQLDAVBcmNvdDEOMAwGA1UECgwFQXJjb3QwHhcNMTcwNjI4MDAwMDAxWhcNMzcwNjIzMjM1OTU5WjA2MRMwEQYKCZImiZPyLGQBAQwDMTYxMQ8wDQYDVQQKDAZDQURJUjExDjAMBgNVBAMMBVNDT1RUMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDFoa5vyyOidbRNnK/92XYrTnbsqI/wc0zjDdxEXEJv38dpqRGeLLeX3OFdyktxAOVgyXLtsvHcaNh6xWrgWY1yh5ANlqxKX1jOl4i//MvuQCu4FI2oXwLn6VYuMupb4aTKwjXN364WqCrz5qGzxmzbK1tq+E/V3c3t8P5hSOTyywIDAQABMA0GCSqGSIb3DQEBBAUAA4GBANWSZSuj0MVkXkRddqrW3xpedcX06RMAWObcxi++gPfaEsVRwErTGktEdYeRErUDtFPYFyINLXi5UTVdOTvUoBgd4UPzXSqLwbf+9+Mvr/2NmmeN05E5ry+de1CN1aAP00tQrjJuPUI1n82qzraLm2hXR4EONJBBl+exoELgF7R5ozUwExYOYWlkYXR0cnZlcnNpb24EATAwDRYDT3JnBAZDQURJUjEwDxYGdXNlcmlkBAVTQ09UVDAA";


    public static AIDFragment newInstance() {
        AIDFragment fragment = new AIDFragment();
        return fragment;
    }

    public AIDFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        USERNAME_POSTFIX = ApplicationConstants.ORGNAME + ApplicationConstants.NAMESPACE;//getResources().getString(R.string.aa_orgname)+getResources().getString(R.string.aa_namespace);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.aid_fragment, container, false);

        Config HOSTNAME = new Config(true, MobileSsoConfig.PROP_TOKEN_HOSTNAME, "server.hostname", String.class);

        // Provision Request Test
        final EditText base64aid = (EditText) view.findViewById(R.id.base64aid);
        final EditText provisionURL = (EditText) view.findViewById(R.id.provision_url);
        final EditText provisionResponse = (EditText) view.findViewById(R.id.provisionresponse);

        base64aid.setText(this.base64aid);
        setScrollToEditText(base64aid);
        setScrollToEditText(provisionResponse);
        ((Button) view.findViewById(R.id.provision_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    provisionResponse.setText("");
                    EditText deviceIdet = (EditText)view.findViewById(R.id.device_id_aid_et) ;
                    String deviceId;
                    if ("".equals(deviceIdet.getEditableText().toString())) {
                        deviceId = null;
                    } else {
                        deviceId = deviceIdet.getEditableText().toString();
                    }
                    AIDAccount account = MASAuthID.getInstance().provisionAIDAccount(base64aid.getEditableText().toString(),
                            provisionURL.getEditableText().toString(), deviceId);

                    if (account != null) {
                        String att = "";
                        try {
                            att = account.getAttribute("AID_PROFILE");
                        } catch (AIDException e) {
                            e.printStackTrace();
                        }
                        provisionResponse.setText(getResources().getString(R.string.success) + ",\n ID : " + account.getId() + " \n User Name " + account.accountId + "\n profile : " + att);
                    } else {
                        provisionResponse.setText(getResources().getString(R.string.failed));
                    }
                } catch (MASAuthIDException e) {
                    provisionResponse.setText(e.getResponse().toString());
                } catch (Exception e) {
                    provisionResponse.setText(e.getMessage());
                }
            }
        });

        // LoginWithSmSession Test
        final EditText smsessionUserID = (EditText) view.findViewById(R.id.smsession_userid);
        final EditText smsessionPassword = (EditText) view.findViewById(R.id.smsession_password);
        final EditText smsessionResponse = (EditText) view.findViewById(R.id.smsession_response);

        setScrollToEditText(smsessionResponse);

        Button verifySmSession = (Button) view.findViewById(R.id.verify_smsession_button);
        verifySmSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String smSessionRespose = smsessionResponse.getEditableText().toString();
                try {
                    JSONObject jsonObject = new JSONObject(smSessionRespose);
                    if (jsonObject.has("smCookie")) {
                        String smCookie = jsonObject.getString("smCookie");
                        Intent intent = new Intent(getActivity(), WebViewActivity.class);
                        intent.putExtra("COOKIE", smCookie);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                }
            }
        });
        doMAGLogincb = (CheckBox) view.findViewById(R.id.smsession_do_mag_login_cb);
        doMAGLogincb.setText("Do MAG Login");
        Button getSMSession = (Button) view.findViewById(R.id.smsession_button);

        getSMSession.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        smsessionResponse.setText("");
                                                    }
                                                });
                                                doMAGLogin = true;
                                                if (doMAGLogincb.isChecked()) {
                                                    doMAGLogin = true;
                                                } else {
                                                    doMAGLogin = false;
                                                }

                                                MASAuthID.getInstance().loginWithAIDGetSMSession(smsessionUserID.getEditableText().toString() + USERNAME_POSTFIX,
                                                        smsessionPassword.getEditableText().toString(), doMAGLogin, new MASCallback<JSONObject>() {
                                                            @Override
                                                            public void onSuccess(final JSONObject jsonObject) {
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                                                @Override
                                                                                                public void run() {
                                                                                                    Log.d("Testing", "loginWIthIDToken succes");
                                                                                                    smsessionResponse.setText(jsonObject.toString());
                                                                                                }
                                                                                            }
                                                                );
                                                            }

                                                            @Override
                                                            public void onError(final Throwable throwable) {
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        if (throwable.getCause() instanceof MASAuthIDException) {
                                                                            smsessionResponse.setText(((MASAuthIDException) throwable.getCause()).getResponse().toString());
                                                                        } else {
                                                                            Log.d("Testing", "-- " + (throwable.getCause()));
                                                                            smsessionResponse.setText(throwable.getMessage());
                                                                        }
                                                                    }
                                                                });
                                                            }

                                                        });
                                            }
                                        }
        );

        // LoginWithTokenID Test
        final EditText tokenIDUserID = (EditText) view.findViewById(R.id.tokenID_userid);
        final EditText tokenIDPassword = (EditText) view.findViewById(R.id.tokenID_password);
        tokenIDResponse = (EditText) view.findViewById(R.id.tokenID_response);

        setScrollToEditText(tokenIDResponse);

//        final String tokenIDResponse = "";
        Button tokenIDButton = (Button) view.findViewById(R.id.tokenID_button);

        tokenIDButton.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 getActivity().runOnUiThread(new Runnable() {
                                                     @Override
                                                     public void run() {
                                                         AIDFragment.this.tokenIDResponse.setText("");
                                                     }
                                                 });
                                                 MASAuthID.getInstance().loginWithAID(tokenIDUserID.getEditableText().toString() + USERNAME_POSTFIX,
                                                         tokenIDPassword.getEditableText().toString(), new MASCallback<MASUser>() {
                                                             @Override
                                                             public void onSuccess(final MASUser masUser) {
                                                                 getActivity().runOnUiThread(new Runnable() {
                                                                                                 @Override
                                                                                                 public void run() {
                                                                                                     AIDFragment.this.tokenIDResponse.setText("Success");
                                                                                                 }
                                                                                             }
                                                                 );
                                                             }

                                                             @Override
                                                             public void onError(final Throwable throwable) {
                                                                 getActivity().runOnUiThread(new Runnable() {
                                                                     @Override
                                                                     public void run() {
                                                                         if (throwable.getCause() instanceof MASAuthIDException) {
                                                                             AIDFragment.this.tokenIDResponse.setText(((MASAuthIDException) throwable.getCause()).getResponse().toString());
                                                                         } else {
                                                                             AIDFragment.this.tokenIDResponse.setText(throwable.getMessage());
                                                                         }
                                                                     }
                                                                 });
                                                             }


                                                         });
                                             }
                                         }
        );


        // GetAccount Request Test
        final EditText getAccountUserID = (EditText) view.findViewById(R.id.get_pki_account_userID);
        final EditText getAccountResponse = (EditText) view.findViewById(R.id.get_pki_account_response);

        setScrollToEditText(getAccountResponse);
        final Button getAccountButton = (Button) view.findViewById(R.id.get_pki_account_button);
        getAccountButton.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    try {
                                                        getAccountResponse.setText("");
                                                        AIDAccount account = MASAuthID.getInstance().getAIDAccount(getAccountUserID.getEditableText().toString() + USERNAME_POSTFIX);
                                                        if (account != null) {
                                                            getAccountResponse.setText(account.getId());
                                                        } else {
                                                            getAccountResponse.setText(getResources().getString(R.string.failed));
                                                        }
                                                    } catch (MASAuthIDException e) {
                                                        getAccountResponse.setText(e.getResponse().toString());
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
        );

        // GetAllAccounts Request Test
        final EditText getAllAccountsResponse = (EditText) view.findViewById(R.id.getAllaccounts_response);
        setScrollToEditText(getAllAccountsResponse);
        Button getAllAccountsButton = (Button) view.findViewById(R.id.getAllAccounts_button);
        getAllAccountsButton.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        try {
                                                            getAllAccountsResponse.setText("");
                                                            AIDAccount[] accounts = MASAuthID.getInstance().getAllAIDAccounts();

                                                            if (accounts != null && accounts.length > 0) {

                                                                JSONArray jsonArray = new JSONArray();

                                                                for (AIDAccount account : accounts) {
                                                                    try {
                                                                        JSONObject jsonObject = new JSONObject();
                                                                        jsonObject.put("User ID", account.getId());
                                                                        jsonObject.put("UserName", account.name);
                                                                        jsonArray.put(jsonObject);
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                                getAllAccountsResponse.setText(jsonArray.toString());
                                                            } else {
                                                                getAllAccountsResponse.setText(getResources().getString(R.string.no_accounts));
                                                            }
                                                        } catch (MASAuthIDException e) {
                                                            getAllAccountsResponse.setText(e.getResponse().toString());
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }

        );


        // Remove Account Request Test
        final EditText removeAccountUserID = (EditText) view.findViewById(R.id.removeAccount_userID);
        final EditText removeAccountResponse = (EditText) view.findViewById(R.id.removeAccount_response);
        Button removeAccountButton = (Button) view.findViewById(R.id.removeAccount_button);
        removeAccountButton.setOnClickListener(new View.OnClickListener() {
                                                   @Override
                                                   public void onClick(View v) {
                                                       try {
                                                           MASAuthID.getInstance().removeAIDAccount(removeAccountUserID.getEditableText().toString() + USERNAME_POSTFIX);
                                                           removeAccountResponse.setText(getResources().getString(R.string.success));
                                                       } catch (MASAuthIDException e) {
                                                           removeAccountResponse.setText(e.getResponse().toString());
                                                       }
                                                   }
                                               }

        );

        final EditText logout_response = (EditText) view.findViewById(R.id.aid_logout_response);


        Button unregisterButton = (Button) view.findViewById(R.id.aid_unregister_device_button);
        unregisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout_response.setText("");
                MASDevice.getCurrentDevice().deregister(new MASCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        //showMessage("Server Registration Removed for This Device", Toast.LENGTH_SHORT);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                logout_response.setText(getResources().getString(R.string.success) + " : Unregistered");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {
                        logout_response.setText(getResources().getString(R.string.failed) + "::" + e.getMessage());
                    }
                });
            }
        });

        Button deleteTokenStoreButton = (Button) view.findViewById(R.id.aid_delete_token_store_btn);
        deleteTokenStoreButton.setOnClickListener(new View.OnClickListener() {
                                                      @Override
                                                      public void onClick(View v) {
                                                          MASDevice.getCurrentDevice().resetLocally();
                                                          getActivity().runOnUiThread(new Runnable() {
                                                              @Override
                                                              public void run() {
                                                                  logout_response.setText(getResources().getString(R.string.success) + " : Reset complete");
                                                              }
                                                          });
                                                      }


                                                  }
        );
        Button logoutButton = (Button) view.findViewById(R.id.aid_logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout_response.setText("");

                if (MASUser.getCurrentUser() != null) {
                    MASUser.getCurrentUser().logout(new MASCallback<Void>() {
                        @Override
                        public void onSuccess(Void result) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    logout_response.setText(getResources().getString(R.string.success));
                                }
                            });
                        }

                        @Override
                        public void onError(Throwable e) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    logout_response.setText(getResources().getString(R.string.failed));
                                }
                            });

                        }
                    });
                } else {
                    logout_response.setText(getResources().getString(R.string.failed));
                }


            }
        });


        protectedAPI_Response = (EditText) view.findViewById(R.id.protectedAPI_response);
        Button access_protectedAPI = (Button) view.findViewById(R.id.protectedAPI_button);

        access_protectedAPI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Testing", "Before InvokeGet products");
                invokeGetProducts();
            }
        });

        MAS.setAuthenticationListener(new MASAuthenticationListener() {

            @Override
            public void onAuthenticateRequest(Context context, long requestId, MASAuthenticationProviders providers) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            protectedAPI_Response.setText("Authentication Required");
                        }
                    });
                }
            }

            @Override
            public void onOtpAuthenticateRequest(Context context, MASOtpAuthenticationHandler handler) {
            }
        });

        setSwitchFunctionality(view);
        return view;

    }


    public void invokeGetProducts() {
        Log.d("Testing", "Inside InvokeGet products");
        final MASRequest request = new MASRequest.MASRequestBuilder(getProductListDownloadUri()).build();
        MAS.invoke(request, new MASCallback<MASResponse<JSONObject>>() {

            @Override
            public Handler getHandler() {
                return new Handler(Looper.getMainLooper());
            }

            @Override
            public void onSuccess(MASResponse<JSONObject> result) {

                protectedAPI_Response.setText(result.getBody().getContent().toString());
            }

            @Override
            public void onError(Throwable e) {
                if (e.getCause() instanceof TargetApiException) {
                    protectedAPI_Response.setText(new String(((TargetApiException) e.getCause()).getResponse()
                            .getBody().getRawContent()));
                } else {
                    protectedAPI_Response.setText(getResources().getString(R.string.failed));
                }
            }
        });
    }

    private URI getProductListDownloadUri() {
        try {
            return new URI("/protected/resource/products?operation=listProducts");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static void setScrollToEditText(final EditText editText) {
     /*   editText.setMovementMethod(new ScrollingMovementMethod());
        ScrollingMovementMethod.getInstance();

        editText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                editText.getParent().requestDisallowInterceptTouchEvent(true);

                return false;
            }


        });*/
    }

    private void setSwitchFunctionality(final View view) {

        SwitchCompat provision = (SwitchCompat) view.findViewById(R.id.provisioningSwitch);
        provision.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    view.findViewById(R.id.base64aidrelativelayout).setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    view.findViewById(R.id.base64aidrelativelayout).setVisibility(View.GONE);
                }
            }
        });


        SwitchCompat access_api = (SwitchCompat) view.findViewById(R.id.accessProtectedAPISwitch);
        access_api.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    view.findViewById(R.id.access_protectedAPI_layout).setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    view.findViewById(R.id.access_protectedAPI_layout).setVisibility(View.GONE);
                }
            }
        });


        SwitchCompat login_aid = (SwitchCompat) view.findViewById(R.id.loginWithAIDSwitch);
        login_aid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    view.findViewById(R.id.loginWithTokenID).setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    view.findViewById(R.id.loginWithTokenID).setVisibility(View.GONE);
                }
            }
        });
        SwitchCompat get_smsession = (SwitchCompat) view.findViewById(R.id.loginWithSMSessionSwitch);
        get_smsession.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    view.findViewById(R.id.loginWithSmSession).setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    view.findViewById(R.id.loginWithSmSession).setVisibility(View.GONE);
                }
            }
        });
        SwitchCompat get_account = (SwitchCompat) view.findViewById(R.id.getAccountSwitch);
        get_account.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    view.findViewById(R.id.getAccountLayout).setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    view.findViewById(R.id.getAccountLayout).setVisibility(View.GONE);
                }
            }
        });
        SwitchCompat remove_account = (SwitchCompat) view.findViewById(R.id.removeAccountSwitch);
        remove_account.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    view.findViewById(R.id.removeAccountLayout).setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    view.findViewById(R.id.removeAccountLayout).setVisibility(View.GONE);
                }
            }
        });
        SwitchCompat get_all_account = (SwitchCompat) view.findViewById(R.id.getAllAccountSwitch);
        get_all_account.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    view.findViewById(R.id.getAllAccountsLayout).setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    view.findViewById(R.id.getAllAccountsLayout).setVisibility(View.GONE);
                }
            }
        });
        SwitchCompat logout = (SwitchCompat) view.findViewById(R.id.aidLogoutwitch);
        logout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    view.findViewById(R.id.aid_logoutLayout).setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    view.findViewById(R.id.aid_logoutLayout).setVisibility(View.GONE);
                }
            }
        });
    }

}
