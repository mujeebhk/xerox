package com.example.ras;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ras.util.AppUtil;
import com.example.ras.util.ApplicationConstants;

public class SettingsActivity extends AppCompatActivity {

    EditText strongAuthOrgNameet, strongAuthNamespaceet;
    Activity activity;
    Button updateSettingsBtn;
    SwitchCompat fidoLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_settings);
        activity = this;
        updateSettingsBtn = (Button) findViewById(R.id.updateSettingsBtn);
        strongAuthOrgNameet = (EditText) findViewById(R.id.strongAuthOrgNameet);
        strongAuthNamespaceet = (EditText) findViewById(R.id.strongAuthNamespaceet);

        strongAuthOrgNameet.setText(ApplicationConstants.ORGNAME);
        strongAuthNamespaceet.setText(ApplicationConstants.NAMESPACE);

        fidoLogin = (SwitchCompat)findViewById(R.id.fidologin);
        fidoLogin.setChecked(ApplicationConstants.FIDO_LOGIN);

        updateSettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApplicationConstants.ORGNAME = strongAuthOrgNameet.getText().toString();
                ApplicationConstants.NAMESPACE = strongAuthNamespaceet.getText().toString();
                ApplicationConstants.FIDO_LOGIN = fidoLogin.isChecked();

                AppUtil.updateAppConstants(activity);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "Settings updated", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


        /*strongAuthOrgNameet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ApplicationConstants.ORGNAME = s.toString();
                AppUtil.updateAppConstants(activity);
            }
        });

        strongAuthNamespaceet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ApplicationConstants.NAMESPACE= s.toString();
                AppUtil.updateAppConstants(activity);
            }
        });
*/

        super.onCreate(savedInstanceState);

    }
}
