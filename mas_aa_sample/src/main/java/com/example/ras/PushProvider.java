package com.example.ras;

import com.ca.sec.aa.mobileAuth.lib.AuthentcatorDelegate;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by gorve01 on 11/14/2016.
 */
public class PushProvider implements AuthentcatorDelegate {

    String refreshedToken = null;

    public String getDeviceToken() {
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        return refreshedToken;
    }

}
