package com.example.ras;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.Toast;

import com.ca.mas.foundation.MASCallback;
import com.ca.mas.foundation.MASUser;
import com.example.ras.util.ApplicationConstants;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {



    private String[] mScreens;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    public static final String TAG = "BaseActivity";
    public static final String ACTIVITYMESSAGE = "ACTIVITYMESSAGE";
    MenuItem logouti, lockApp, fundTransferi, billPaymenti, cardsi, logini, homeScreeni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (ApplicationConstants.GOTO_DEVICE_HOME_SCREEN) {
            ApplicationConstants.GOTO_DEVICE_HOME_SCREEN = false;
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }

        Bundle extras = getIntent().getExtras();
        if(extras !=null && extras.getString(ACTIVITYMESSAGE) != null) {
            showToastMessage(extras.getString(ACTIVITYMESSAGE));
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        View rootView = findViewById(android.R.id.content);
        rootView.setBackgroundResource(R.mipmap.background);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        try {
            Menu menu = navigationView.getMenu();
            logouti = menu.findItem(R.id.logouti);
            lockApp = menu.findItem(R.id.lockAppi);
            fundTransferi = menu.findItem(R.id.fundTransferi);
            billPaymenti = menu.findItem(R.id.billPaymenti);
            cardsi = menu.findItem(R.id.cardsi);
            logini = menu.findItem(R.id.loginScreeni);
            homeScreeni = menu.findItem(R.id.home_screen1);

            logini.setEnabled(true);
            lockApp.setEnabled(false);
            logouti.setEnabled(false);
            fundTransferi.setEnabled(false);
            billPaymenti.setEnabled(false);
            cardsi.setEnabled(false);
            homeScreeni.setEnabled(false);


            MASUser user = MASUser.getCurrentUser();
            if (user != null && user.isAuthenticated()) {
                logouti.setEnabled(true);
                logouti.setEnabled(true);
                fundTransferi.setEnabled(true);
                billPaymenti.setEnabled(true);
                cardsi.setEnabled(true);
                logini.setEnabled(false);
                homeScreeni.setEnabled(true);
            }
            if (user != null && !user.isSessionLocked()) {
                lockApp.setEnabled(true);
            }


        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
    }

    public void showToastMessage(final String msg) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logouti) {
            if (MASUser.getCurrentUser() != null) {
                MASUser.getCurrentUser().logout(new MASCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
            }

        } else if (id == R.id.lockAppi) {
            if (MASUser.getCurrentUser() != null) {
                MASUser.getCurrentUser().lockSession(new MASCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
            }

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else if (id == R.id.fundTransferi) {
            ApplicationConstants.IS_FUND_TRANSFER = true;
            Intent intent = new Intent(this, FundTransferActivity.class);
            startActivity(intent);
        } else if (id == R.id.loginScreeni) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else if (id == R.id.home_screen1) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        } /*else if (id == R.id.home_screen2) {
            Intent intent = new Intent(this, WebViewActivity.class);
            startActivity(intent);
        }*/ else if (id == R.id.credentials) {
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
        }else if (id == R.id.pushnotification) {
            Intent intent = new Intent(this, AuthenticatorActivity.class);
            startActivity(intent);
        } else if (id == R.id.settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.registerForStrongAuthi) {
            Intent intent = new Intent(this, SelectRegistrationCredActivity.class);
            startActivity(intent);
        } else if (id == R.id.billPaymenti) {
            ApplicationConstants.IS_FUND_TRANSFER = false;
            Intent intent = new Intent(this, FundTransferActivity.class);
            startActivity(intent);
        } else if (id == R.id.cardsi) {
            Intent intent = new Intent(this, CardsActivity.class);
            startActivity(intent);
        } else if (id == R.id.branchesi) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationConstants.BRANCHES_NEARBY_URL));
            startActivity(browserIntent);
        } else if (id == R.id.atmi) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationConstants.ATM_NEARBY_URL));
            startActivity(browserIntent);
        }

        try {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    public void putDataInIntentBundle(Intent intent, String message) {
        intent.putExtra(ACTIVITYMESSAGE,message);
    }


}
