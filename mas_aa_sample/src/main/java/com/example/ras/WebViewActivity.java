package com.example.ras;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.ca.mas.core.error.TargetApiException;
import com.ca.mas.foundation.MAS;
import com.ca.mas.foundation.MASCallback;
import com.ca.mas.foundation.MASDevice;
import com.ca.mas.foundation.MASRequest;
import com.ca.mas.foundation.MASResponse;
import com.ca.mas.foundation.MASUser;
import com.ca.mas.foundation.auth.MASProximityLoginQRCode;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class WebViewActivity extends BaseActivity {
    private static final String TAG = "WebViewActivity";

    private WebView webView;

    Activity context;
    Button detailBtn, logoutbtn, qrcodebtn;
    ListView itemList;

    private static final int MENU_GROUP_LOGOUT = 66;
    private static final int MENU_ITEM_LOG_OUT = 3;
    private static final int MENU_ITEM_REMOVE_DEVICE_REGISTRATION = 4;
    private static final int MENU_ITEM_DESTROY_TOKEN_STORE = 2;



    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_webview);

        context = this;
        itemList = (ListView) findViewById(R.id.itemList);

        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webView.setWebChromeClient(new WebChromeClient());

        //webView.loadUrl("https://www.7-eleven.com/7rewards");

        /*getRewardsBtn = (Button) findViewById(R.id.getRewardsBtn);
        getRewardsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final MASRequest request = new MASRequest.MASRequestBuilder(getProductListDownloadUri()).build();

                MAS.invoke(request, new MASCallback<MASResponse<JSONObject>>() {
                    @Override
                    public Handler getHandler() {
                        return new Handler(Looper.getMainLooper());
                    }

                    @Override
                    public void onSuccess(MASResponse<JSONObject> result) {
                        webView.loadUrl("https://www.ca.com/us.html");
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.getCause() instanceof TargetApiException) {
                            showMessage(new String(((TargetApiException) e.getCause()).getResponse()
                                    .getBody().getRawContent()), Toast.LENGTH_SHORT);
                        } else {
                            showMessage("Error: " + e.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                });

            }
        });
*/

        detailBtn = (Button) findViewById(R.id.credDetailsBtn);
        detailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DashboardActivity.class);
                startActivity(intent);
            }
        });

        qrcodebtn = (Button) findViewById(R.id.scanQrCodeButton);
        qrcodebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(context);
                intentIntegrator.initiateScan();
            }
        });

        final Button logOutButton = (Button) findViewById(R.id.logoutButton);
        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doServerLogout();
            }
        });
        registerForContextMenu(logOutButton);



        final Button listButton = (Button) findViewById(R.id.listItemsButton);
        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearItem();
                //Android M Permission
                checkPermission();

                final MASRequest request = new MASRequest.MASRequestBuilder(getProductListDownloadUri()).build();

                MAS.invoke(request, new MASCallback<MASResponse<JSONObject>>() {
                    @Override
                    public Handler getHandler() {
                        return new Handler(Looper.getMainLooper());
                    }

                    @Override
                    public void onSuccess(MASResponse<JSONObject> result) {
                        setDownloadedJson(result.getBody().getContent());
                        try {
                            List<Object> objects = parseProductListJson(result.getBody().getContent());
                            ArrayAdapter arrayAdapter = new ArrayAdapter<Object>(WebViewActivity.this, R.layout.listitem, objects);
                            itemList.setAdapter(arrayAdapter);
                            arrayAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            showMessage("Error: " + e.getMessage(), Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.getCause() instanceof TargetApiException) {
                            showMessage(new String(((TargetApiException) e.getCause()).getResponse()
                                    .getBody().getRawContent()), Toast.LENGTH_SHORT);
                        } else {
                            showMessage("Error: " + e.getMessage(), Toast.LENGTH_LONG);
                        }
                    }
                });
            }
        });

        super.onCreate(savedInstanceState);

    }

    private URI getProductListDownloadUri() {
        try {
            return new URI("/protected/resource/products?operation=listProducts");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }


    private void clearItem() {
        ArrayAdapter a = ((ArrayAdapter) itemList.getAdapter());
        if (a != null) {
            a.clear();
            a.notifyDataSetChanged();
        }
    }


    public void showMessage(final String message, final int toastLength) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(WebViewActivity.this, message, toastLength).show();
            }
        });
    }



    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        0);
            }
            if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        0);
            }
        }
    }


    public void setDownloadedJson(JSONObject json) {

    }

    private static List<Object> parseProductListJson(JSONObject json) throws JSONException {
        try {
            List<Object> objects = new ArrayList<Object>();
            JSONArray items = json.getJSONArray("products");
            for (int i = 0; i < items.length(); ++i) {
                JSONObject item = (JSONObject) items.get(i);
                Integer id = (Integer) item.get("id");
                String name = (String) item.get("name");
                objects.add(new Pair<Integer, String>(id, name) {
                    @Override
                    public String toString() {
                        return first + "  " + second;
                    }
                });
            }
            return objects;
        } catch (ClassCastException e) {
            throw (JSONException) new JSONException("Response JSON was not in the expected format").initCause(e);
        }
    }



   @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(MENU_GROUP_LOGOUT, MENU_ITEM_LOG_OUT, Menu.NONE, "Log Out");
        menu.add(MENU_GROUP_LOGOUT, MENU_ITEM_REMOVE_DEVICE_REGISTRATION, Menu.NONE, "Unregister Device");
        menu.add(MENU_GROUP_LOGOUT, MENU_ITEM_DESTROY_TOKEN_STORE, Menu.NONE, "Destroy Token Store");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (66 != item.getGroupId())
            return false;
        switch (item.getItemId()) {
            case MENU_ITEM_DESTROY_TOKEN_STORE:
                MASDevice.getCurrentDevice().resetLocally();
                showMessage("Device Registration Destroyed (client only)", Toast.LENGTH_SHORT);
                return true;
            case MENU_ITEM_LOG_OUT:
                doServerLogout();
                return true;
            case MENU_ITEM_REMOVE_DEVICE_REGISTRATION:
                doServerUnregisterDevice();
                return true;
        }
        return false;
    }

    // Log the user out of all client apps and notify the server to revoke tokens.
    private void doServerLogout() {
        clearItem();
        if (MASUser.getCurrentUser() != null) {
            MASUser.getCurrentUser().logout(new MASCallback<Void>() {
                @Override
                public void onSuccess(Void result) {
                    showMessage("Successful Logout", Toast.LENGTH_SHORT);
                }

                @Override
                public void onError(Throwable e) {
                    showMessage("Fail Logout", Toast.LENGTH_SHORT);
                }
            });
        }
    }

    // Tell the token server to un-register this device, without affecting the client-side token caches in any way.
    private void doServerUnregisterDevice() {
        MASDevice.getCurrentDevice().deregister(new MASCallback<Void>() {
            @Override
            public void onSuccess(Void result) {
                showMessage("Server Registration Removed for This Device", Toast.LENGTH_SHORT);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                showMessage(e.getMessage(), Toast.LENGTH_SHORT);
            }
        });
    }


    /**
     * Process remote login through QRCode
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Got the QR Code, perform the remote login request.
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            String r = scanResult.getContents();
            if (r != null) {
                MASProximityLoginQRCode.authorize(r, new MASCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                    }

                    @Override
                    public void onError(Throwable e) {
                        showMessage(e.getMessage(), Toast.LENGTH_LONG);
                    }
                });
            }
        }
    }

}
