package com.ca.apim.mas.authotp;

import android.content.SharedPreferences;
import android.util.Log;

import com.arcot.aotp.lib.OTP;
import com.arcot.aotp.lib.store.DeviceLock;
import com.ca.mas.core.context.DeviceIdentifier;
import com.ca.mas.foundation.MAS;

import org.json.JSONException;

/**
 * Created by mujmo02 on 9/15/2017.
 */

public class MASAOTPUtil {

    private static final String TAG = "MASAOTPUtil";
    static public String getPrintableErrorMessage (Throwable e) {
        if (e instanceof MASAuthOTPException) {
            try {
                return ((MASAuthOTPException) e).getResponse().getString("error_details");
            } catch (JSONException e1) {
                e.printStackTrace();
            }
        }
        return e.getMessage();
    }

    private static final String PREFS_NAME = "DEVICE_ID_AUTHOTP";

    static void persistData(String key, String value) {
        key = key.toLowerCase();
        Log.d(TAG, "Persisting data  Key :" + key + "  value :"+value);

        SharedPreferences settings = MAS.getCurrentActivity().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);

        // Commit the edits!
        editor.commit();
    }

    static String getPersistedData(String key) {
        key = key.toLowerCase();
        SharedPreferences settings = MAS.getCurrentActivity().getSharedPreferences(PREFS_NAME, 0);
        Log.d(TAG, "Get Persisting data  Key :" + key + "  value :"+settings.getString(key, null));
        return settings.getString(key, null);

    }

    static void deleteAllEntriesInPersistedData () {
        SharedPreferences settings = MAS.getCurrentActivity().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
        Log.d(TAG, "deleteAllEntriesInPersistedData" );

    }

    static void deleteEntryInPersistedData (String key) {
        key = key.toLowerCase();
        SharedPreferences settings = MAS.getCurrentActivity().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
        Log.d(TAG, "deleteEntryInPersistedData  Key :" + key );
    }

    static void setDeviceLocking (final String deviceId, OTP otp) {

        DeviceLock dl = new DeviceLock() {
            @Override
            public String getKey() {

                return deviceId;
            }
        };
        otp.setDeviceLock(dl);
    }
}
